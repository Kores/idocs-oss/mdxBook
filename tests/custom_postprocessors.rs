mod dummy_book;

use crate::dummy_book::DummyBook;
use mdxbook::postprocess::{CmdPostprocessor, Postprocessor};
use mdxbook::MDBook;

fn example() -> CmdPostprocessor {
    CmdPostprocessor::new(
        "nop-postprocessor".to_string(),
        "cargo run --example nop-postprocessor --".to_string(),
    )
}

#[test]
fn example_supports_whatever() {
    let cmd = example();

    let got = cmd.supports_renderer("whatever");

    assert_eq!(got, true);
}

#[test]
fn example_doesnt_support_not_supported() {
    let cmd = example();

    let got = cmd.supports_renderer("not-supported");

    assert_eq!(got, false);
}

#[test]
fn ask_the_postprocessor_to_blow_up() {
    let dummy_book = DummyBook::new();
    let temp = dummy_book.build().unwrap();
    let mut md = MDBook::load(temp.path()).unwrap();
    md.with_postprocessor(example());

    md.config
        .set("postprocessor.nop-postprocessor.blow-up", true)
        .unwrap();

    let got = md.build();

    assert!(got.is_err());
}

#[test]
fn process_the_dummy_book() {
    let dummy_book = DummyBook::new();
    let temp = dummy_book.build().unwrap();
    let mut md = MDBook::load(temp.path()).unwrap();
    md.with_postprocessor(example());

    md.build().unwrap();
}
