use assert_cmd::Command;

pub(crate) fn mdxbook_cmd() -> Command {
    let mut cmd = Command::cargo_bin("mdxbook").unwrap();
    cmd.env_remove("RUST_LOG");
    cmd
}
