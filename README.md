# mdxBook

A modified version of [mdBook](https://github.com/rust-lang/mdBook) which empowers the [idocs blog](https://blog.idocs.xyz)

## Description

[mdBook](https://github.com/rust-lang/mdBook) is a very interesting tool for generating documentation pages,
in [my blog](https://blog.idocs.xyz) I used it to generate the blog pages, however, some changes need to be made
to achieve the needs of the blog without doing a bunch of hacks.

This repository contains all those modifications.

## Modifications

- Allow custom Template Variables per `Book` and per `Chapter`.
- Allow custom post-processors.
- Remove all files included directly on the `mdBook` binary to reduce it's size.

## Post Processors

[mdBook](https://github.com/rust-lang/mdBook) provides a way to add **Pre Processors** to your book creation workflow,
they work on the **Markdown-to-Markdown** step, before it's rendered using the configured renderer.

**Post processors** work at the HTML level (or rendered file level), processing the produced HTML/file
before writing it to disk. A workaround for one that is using [mdBook](https://github.com/rust-lang/mdBook)
as a library, is to read these HTML files and apply those modifications, but this is not as integrated as the **Pre Processors**,
and introduce more complexities, like knowing the output directory, which files you can overwrite, and other configuration
details already available during the rendering process.

These **post processors** has access to rendering context and other information provided by the backend. **Post processors**
are backend dependant, this means that they are unlikely to work with a different backend they are designed to, for comparison,
**Pre processors** are likely to work with different backends, since they transform the Markdown and not the produced output.

## Note

Since one of those modifications removes all the files that are included directly in the
compiled binary, to reduce the binary size, the binaries produced by this fork requires all
the theme files to be present in the binary root directory.

Also, it's important to know that those modifications are only the modifications made to [mdBook](https://github.com/rust-lang/mdBook),
and does not include any source of the blog binary, which is a separate project that only add [mdxBook] as dependency.

And, some adaptations were made to original code, so updating to upstream may take sometime to fix merge conflicts.

Those changes were not proposed to [mdBook](https://github.com/rust-lang/mdBook) because they serve a purpose: facilitate
the use of mdBook as a library, and not as a binary. mdBook is focused on end-users, install and run, without any additional configuration,
**mdxBook** is not.

## Licensing

This code is licensed under the same license as [mdBook](https://github.com/rust-lang/mdBook), the [MPL 2.0](https://www.mozilla.org/en-US/MPL/2.0/),
which can be found in the root of this project.