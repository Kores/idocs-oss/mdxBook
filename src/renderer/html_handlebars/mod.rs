#![allow(missing_docs)] // FIXME: Document this

pub use self::hbs_renderer::HtmlHandlebars;
pub use self::hbs_renderer::{hide_lines, partition_source};

mod hbs_renderer;
mod helpers;

#[cfg(feature = "search")]
mod search;
