//! Theme dependencies for in-browser search. Not included in mdbook when
//! the "search" cargo feature is disabled.

pub const JS_NAME: &'static str = "searcher/searcher.js";
pub const MARK_JS_NAME: &'static str = "searcher/mark.min.js";
pub const ELASTICLUNR_JS_NAME: &'static str = "searcher/elasticlunr.min.js";
