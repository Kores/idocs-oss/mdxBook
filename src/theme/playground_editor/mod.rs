//! Theme dependencies for the playground editor.

pub const JS_NAME: &'static str = "playground_editor/editor.js";
pub const ACE_JS: &'static str = "playground_editor/ace.js";
pub const MODE_RUST_JS: &'static str = "playground_editor/mode-rust.js";
pub const THEME_DAWN_JS: &'static str = "playground_editor/theme-dawn.js";
pub const THEME_TOMORROW_NIGHT_JS: &'static str = "playground_editor/theme-tomorrow_night.js";
